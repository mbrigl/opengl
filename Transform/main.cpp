#include <iostream>
#include <cmath>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "utils.h"

#define WIDTH 800
#define HEIGHT 600


// The MAIN function, from here we start the application and run the game loop
int main()
{
    GLFWwindow * window = initOpenGL(WIDTH, HEIGHT, "Textures");


    // Let's transform our plane a bit by rotating it on the x-axis
    // so it looks like it's laying on the floor
    //glm::mat4 model;
    //model = glm::rotate(model, glm::radians(-55.0f), glm::vec3(1.0f, 0.0f, 0.0f));

    // Move slightly backwards in the scene so the object becomes visible
    // Note that we're translating the scene in the reverse direction of where we want to move
    glm::mat4 view;
    view = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));

    // use perspective projection for our scene
    glm::mat4 projection;
    projection = glm::perspective(glm::radians(45.0f), WIDTH / (float)HEIGHT,
                                  0.1f, 100.0f);


    // Build and compile our shader program
    GLuint shader = loadShader("/mnt/sda3/GIT/OpenGL/Transform/shader.vert",
                               "/mnt/sda3/GIT/OpenGL/Transform/shader.frag");

    // Load and create textures
    GLuint texture1 = loadTexture("/mnt/sda3/GIT/OpenGL/Texture/container.jpg");
    GLuint texture2 = loadTexture("/mnt/sda3/GIT/OpenGL/Texture/awesomeface.png");


    // Set up vertex data (and buffer(s)) and attribute pointers
    GLfloat vertices[] = {
        -0.5f, -0.5f, -0.5f, +0.0f, +0.0f,
        +0.5f, -0.5f, -0.5f, +1.0f, +0.0f,
        +0.5f, +0.5f, -0.5f, +1.0f, +1.0f,
        +0.5f, +0.5f, -0.5f, +1.0f, +1.0f,
        -0.5f, +0.5f, -0.5f, +0.0f, +1.0f,
        -0.5f, -0.5f, -0.5f, +0.0f, +0.0f,

        -0.5f, -0.5f, +0.5f, +0.0f, +0.0f,
        +0.5f, -0.5f, +0.5f, +1.0f, +0.0f,
        +0.5f, +0.5f, +0.5f, +1.0f, +1.0f,
        +0.5f, +0.5f, +0.5f, +1.0f, +1.0f,
        -0.5f, +0.5f, +0.5f, +0.0f, +1.0f,
        -0.5f, -0.5f, +0.5f, +0.0f, +0.0f,

        -0.5f, +0.5f, +0.5f, +1.0f, +0.0f,
        -0.5f, +0.5f, -0.5f, +1.0f, +1.0f,
        -0.5f, -0.5f, -0.5f, +0.0f, +1.0f,
        -0.5f, -0.5f, -0.5f, +0.0f, +1.0f,
        -0.5f, -0.5f, +0.5f, +0.0f, +0.0f,
        -0.5f, +0.5f, +0.5f, +1.0f, +0.0f,

        +0.5f, +0.5f, +0.5f, +1.0f, +0.0f,
        +0.5f, +0.5f, -0.5f, +1.0f, +1.0f,
        +0.5f, -0.5f, -0.5f, +0.0f, +1.0f,
        +0.5f, -0.5f, -0.5f, +0.0f, +1.0f,
        +0.5f, -0.5f, +0.5f, +0.0f, +0.0f,
        +0.5f, +0.5f, +0.5f, +1.0f, +0.0f,

        -0.5f, -0.5f, -0.5f, +0.0f, +1.0f,
        +0.5f, -0.5f, -0.5f, +1.0f, +1.0f,
        +0.5f, -0.5f, +0.5f, +1.0f, +0.0f,
        +0.5f, -0.5f, +0.5f, +1.0f, +0.0f,
        -0.5f, -0.5f, +0.5f, +0.0f, +0.0f,
        -0.5f, -0.5f, -0.5f, +0.0f, +1.0f,

        -0.5f, +0.5f, -0.5f, +0.0f, +1.0f,
        +0.5f, +0.5f, -0.5f, +1.0f, +1.0f,
        +0.5f, +0.5f, +0.5f, +1.0f, +0.0f,
        +0.5f, +0.5f, +0.5f, +1.0f, +0.0f,
        -0.5f, +0.5f, +0.5f, +0.0f, +0.0f,
        -0.5f, +0.5f, -0.5f, +0.0f, +1.0f
    };

    glm::vec3 cubePositions[] = {
        glm::vec3(0.0f,  0.0f,  0.0f),
        glm::vec3(2.0f,  5.0f, -15.0f),
        glm::vec3(-1.5f, -2.2f, -2.5f),
        glm::vec3(-3.8f, -2.0f, -12.3f),
        glm::vec3(2.4f, -0.4f, -3.5f),
        glm::vec3(-1.7f,  3.0f, -7.5f),
        glm::vec3(1.3f, -2.0f, -2.5f),
        glm::vec3(1.5f,  2.0f, -2.5f),
        glm::vec3(1.5f,  0.2f, -1.5f),
        glm::vec3(-1.3f,  1.0f, -1.5f)
    };

    GLuint VBO, VAO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat),
                          (GLvoid *)0);
    glEnableVertexAttribArray(0);

    // TexCoord attribute
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat),
                          (GLvoid *)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);

    glBindVertexArray(0); // Unbind VAO


    while (!glfwWindowShouldClose(window)) {
        // Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
        glfwPollEvents();

        // Render
        // Clear the colorbuffer
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Activate shader
        glUseProgram(shader);

        // Update the uniform color
        GLint weight = glGetUniformLocation(shader, "weight");
        glUniform1f(weight, getMixValue());


        // Update the matrices
//        glm::mat4 model;
//        model = glm::rotate(model, glm::radians((GLfloat)glfwGetTime() * 50.0f),
//                            glm::vec3(0.5f, 1.0f, 0.0f));
//        glUniformMatrix4fv(glGetUniformLocation(shader, "model"),
//                           1, GL_FALSE, glm::value_ptr(model));
        glUniformMatrix4fv(glGetUniformLocation(shader, "view"),
                           1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(glGetUniformLocation(shader, "projection"),
                           1, GL_FALSE, glm::value_ptr(projection));


        // Bind Textures using texture units (at least a minimum of 16 texture units)
        glActiveTexture(GL_TEXTURE0);  // Activate the texture unit before binding tx
        glBindTexture(GL_TEXTURE_2D, texture1);  // bind tx to the active texture unit
        // bind uniform "texture1" to texture unit 0
        glUniform1i(glGetUniformLocation(shader, "texture1"), 0);
        glActiveTexture(GL_TEXTURE1);  // Activate the texture unit before binding tx
        glBindTexture(GL_TEXTURE_2D, texture2);  // bind tx to the active texture unit
        // bind uniform "texture2" to texture unit 1
        glUniform1i(glGetUniformLocation(shader, "texture2"), 1);

        // Draw container
        glBindVertexArray(VAO);

        for (GLuint i = 0; i < 10; i++) {
            glm::mat4 model;
            model = glm::translate(model, cubePositions[i]);
            GLfloat angle = glm::radians(20.0f) * i;
            model = glm::rotate(model, angle, glm::vec3(1.0f, 0.3f, 0.5f));

            if (i % 4 == 0) {
                model = glm::rotate(model, glm::radians((GLfloat)glfwGetTime() * 50.0f),
                                    glm::vec3(0.5f, 1.0f, 0.0f));
            }

            glUniformMatrix4fv(glGetUniformLocation(shader, "model"),
                               1, GL_FALSE, glm::value_ptr(model));

            glDrawArrays(GL_TRIANGLES, 0, 36);
        }

        glBindVertexArray(0);

        // Swap the screen buffers
        glfwSwapBuffers(window);
    }

    // Properly de-allocate all resources once they've outlived their purpose
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);

    // Terminate GLFW, clearing any resources allocated by GLFW.
    glfwTerminate();
    return 0;
}
