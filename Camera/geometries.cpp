#include "geometries.h"

void cubeTex(GLuint * VAO, GLuint * VBO)
{
    // Set up vertex data (and buffer(s)) and attribute pointers
    GLfloat vertices[] = {
        -0.5f, -0.5f, -0.5f,   +0.0f, +0.0f,
        +0.5f, -0.5f, -0.5f,   +1.0f, +0.0f,
        +0.5f, +0.5f, -0.5f,   +1.0f, +1.0f,
        +0.5f, +0.5f, -0.5f,   +1.0f, +1.0f,
        -0.5f, +0.5f, -0.5f,   +0.0f, +1.0f,
        -0.5f, -0.5f, -0.5f,   +0.0f, +0.0f,

        -0.5f, -0.5f, +0.5f,   +0.0f, +0.0f,
        +0.5f, -0.5f, +0.5f,   +1.0f, +0.0f,
        +0.5f, +0.5f, +0.5f,   +1.0f, +1.0f,
        +0.5f, +0.5f, +0.5f,   +1.0f, +1.0f,
        -0.5f, +0.5f, +0.5f,   +0.0f, +1.0f,
        -0.5f, -0.5f, +0.5f,   +0.0f, +0.0f,

        -0.5f, +0.5f, +0.5f,   +1.0f, +0.0f,
        -0.5f, +0.5f, -0.5f,   +1.0f, +1.0f,
        -0.5f, -0.5f, -0.5f,   +0.0f, +1.0f,
        -0.5f, -0.5f, -0.5f,   +0.0f, +1.0f,
        -0.5f, -0.5f, +0.5f,   +0.0f, +0.0f,
        -0.5f, +0.5f, +0.5f,   +1.0f, +0.0f,

        +0.5f, +0.5f, +0.5f,   +1.0f, +0.0f,
        +0.5f, +0.5f, -0.5f,   +1.0f, +1.0f,
        +0.5f, -0.5f, -0.5f,   +0.0f, +1.0f,
        +0.5f, -0.5f, -0.5f,   +0.0f, +1.0f,
        +0.5f, -0.5f, +0.5f,   +0.0f, +0.0f,
        +0.5f, +0.5f, +0.5f,   +1.0f, +0.0f,

        -0.5f, -0.5f, -0.5f,   +0.0f, +1.0f,
        +0.5f, -0.5f, -0.5f,   +1.0f, +1.0f,
        +0.5f, -0.5f, +0.5f,   +1.0f, +0.0f,
        +0.5f, -0.5f, +0.5f,   +1.0f, +0.0f,
        -0.5f, -0.5f, +0.5f,   +0.0f, +0.0f,
        -0.5f, -0.5f, -0.5f,   +0.0f, +1.0f,

        -0.5f, +0.5f, -0.5f,   +0.0f, +1.0f,
        +0.5f, +0.5f, -0.5f,   +1.0f, +1.0f,
        +0.5f, +0.5f, +0.5f,   +1.0f, +0.0f,
        +0.5f, +0.5f, +0.5f,   +1.0f, +0.0f,
        -0.5f, +0.5f, +0.5f,   +0.0f, +0.0f,
        -0.5f, +0.5f, -0.5f,   +0.0f, +1.0f
    };


    glGenVertexArrays(1, VAO);
    glGenBuffers(1, VBO);

    glBindVertexArray(*VAO);

    glBindBuffer(GL_ARRAY_BUFFER, *VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                          5 * sizeof(GLfloat), (GLvoid *)0);
    glEnableVertexAttribArray(0);

    // TexCoord attribute
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat),
                          (GLvoid *)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);


    glBindVertexArray(0); // Unbind VAO
}
