#include <iostream>
#include <cmath>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "utils.h"
#include "geometries.h"

#define WIDTH 800
#define HEIGHT 600


GLfloat mixValue = 0.2f;
bool keys[1024];
GLfloat deltaTime = 0.0f;	// Time between current frame and last frame
GLfloat lastFrame = 0.0f;  	// Time of last frame
glm::vec3 cameraPos   = glm::vec3(0.0f, 0.0f,  3.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 cameraUp    = glm::vec3(0.0f, 1.0f,  0.0f);

GLfloat lastX = WIDTH / 2, lastY = HEIGHT / 2;
bool firstMouse = true;
// Yaw is initialized to -90.0 degrees since a yaw of 0.0 results
// in a direction vector pointing to the right (due to how Eular angles work)
// so we initially rotate a bit to the left.
GLfloat yaw   = -90.0f;
GLfloat pitch =   0.0f;


// The MAIN function, from here we start the application and run the game loop
int main()
{
    GLFWwindow * window = initOpenGL(WIDTH, HEIGHT, "Camera");


    // use perspective projection for our scene
    glm::mat4 projection = glm::perspective(glm::radians(45.0f),
                                            WIDTH / (float)HEIGHT,
                                            0.1f, 100.0f);


    // Build and compile our shader program
    GLuint shader = loadShader("/mnt/sda3/GIT/OpenGL/Transform/shader.vert",
                               "/mnt/sda3/GIT/OpenGL/Transform/shader.frag");

    // Load and create textures
    GLuint texture1 = loadTexture("/mnt/sda3/GIT/OpenGL/Texture/container.jpg");
    GLuint texture2 = loadTexture("/mnt/sda3/GIT/OpenGL/Texture/awesomeface.png");


    glm::vec3 cubePositions[] = {
        glm::vec3(0.0f,  0.0f,  0.0f),
        glm::vec3(2.0f,  5.0f, -15.0f),
        glm::vec3(-1.5f, -2.2f, -2.5f),
        glm::vec3(-3.8f, -2.0f, -12.3f),
        glm::vec3(2.4f, -0.4f, -3.5f),
        glm::vec3(-1.7f,  3.0f, -7.5f),
        glm::vec3(1.3f, -2.0f, -2.5f),
        glm::vec3(1.5f,  2.0f, -2.5f),
        glm::vec3(1.5f,  0.2f, -1.5f),
        glm::vec3(-1.3f,  1.0f, -1.5f)
    };

    GLuint VBO, VAO;
    cubeTex(&VAO, &VBO);


    while (!glfwWindowShouldClose(window)) {

        // Calculate deltatime of current frame
        GLfloat currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        glfwPollEvents();
        move();
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUseProgram(shader);

        // Update the uniform color
        GLint weight = glGetUniformLocation(shader, "weight");
        glUniform1f(weight, mixValue);

        // enable textures
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture1);
        glUniform1i(glGetUniformLocation(shader, "texture1"), 0);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texture2);
        glUniform1i(glGetUniformLocation(shader, "texture2"), 1);


        // Draw container
        glBindVertexArray(VAO);

        // Update the matrices
        glm::mat4 view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
        glUniformMatrix4fv(glGetUniformLocation(shader, "view"),
                           1, GL_FALSE, glm::value_ptr(view));

        glUniformMatrix4fv(glGetUniformLocation(shader, "projection"),
                           1, GL_FALSE, glm::value_ptr(projection));

        for (GLuint i = 0; i < 10; i++) {
            glm::mat4 model;
            model = glm::translate(model, cubePositions[i]);
            GLfloat angle = glm::radians(20.0f) * i;
            model = glm::rotate(model, angle, glm::vec3(1.0f, 0.3f, 0.5f));
            glUniformMatrix4fv(glGetUniformLocation(shader, "model"),
                               1, GL_FALSE, glm::value_ptr(model));

            glDrawArrays(GL_TRIANGLES, 0, 36);
        }

        glBindVertexArray(0);


        glfwSwapBuffers(window);
    }


    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);

    glfwTerminate();
    return 0;
}


void mouse_callback(GLFWwindow * window, double xpos, double ypos)
{
    if (firstMouse) {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    GLfloat xoffset = xpos - lastX;
    GLfloat yoffset = lastY - ypos;
    lastX = xpos;
    lastY = ypos;

    GLfloat sensitivity = 0.05;
    xoffset *= sensitivity;
    yoffset *= sensitivity;

    yaw   += xoffset;
    pitch += yoffset;

    if (pitch > 89.0f) {
        pitch = 89.0f;
    }

    if (pitch < -89.0f) {
        pitch = -89.0f;
    }

    glm::vec3 front;
    front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    front.y = sin(glm::radians(pitch));
    front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    cameraFront = glm::normalize(front);
}


// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow * window, int key, int scancode,
                  int action, int mode)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }

    // Change value of uniform with arrow keys (sets amount of textre mix)
    if (key == GLFW_KEY_UP && action == GLFW_PRESS) {
        mixValue += 0.1f;

        if (mixValue >= 1.0f) {
            mixValue = 1.0f;
        }
    }

    if (key == GLFW_KEY_DOWN && action == GLFW_PRESS) {
        mixValue -= 0.1f;

        if (mixValue <= 0.0f) {
            mixValue = 0.0f;
        }
    }


    if (action == GLFW_PRESS) {
        keys[key] = true;
    } else if (action == GLFW_RELEASE) {
        keys[key] = false;
    }
}

void move()
{
    GLfloat cameraSpeed = 5.0f * deltaTime;

    if (keys[GLFW_KEY_W]) {
        cameraPos += cameraSpeed * cameraFront;
    }

    if (keys[GLFW_KEY_S]) {
        cameraPos -= cameraSpeed * cameraFront;
    }

    if (keys[GLFW_KEY_A]) {
        cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    }

    if (keys[GLFW_KEY_D]) {
        cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    }

    /* Note that we normalize the resulting right vector.
     * If we wouldn't normalize this vector, the resulting cross product
     * might return differently sized vectors based on the cameraFront variable.
     * If we would not normalize the vector we would either move slow or fast
     * based on the camera's orientation instead of at a consistent movement speed. */
}
