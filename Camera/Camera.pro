TEMPLATE = app
CONFIG += c++11
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lglfw -lGL -lGLEW -lSOIL

SOURCES += \
    main.cpp \
    utils.cpp \
    geometries.cpp

HEADERS += \
    utils.h \
    geometries.h

OTHER_FILES += \
    shader.vert \
    shader.frag
