#version 330 core
in vec2 TexCoord;

out vec4 color;

// Texture samplers
uniform sampler2D texture1;
uniform sampler2D texture2;

uniform float weight;

void main()
{
    // Linearly interpolate between both textures (weight refers to second parameter)
    color = mix(texture(texture1, TexCoord), texture(texture2, TexCoord), weight);
}
