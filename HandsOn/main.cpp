#include <iostream>
#include <math.h>

#include <GL/glew.h>  // OpenGL Extension Wrangler
#include <GL/glut.h>  // OpenGL Utility Toolkit

using namespace std;

double col = 0.5;
double d_col = 0.0003;
bool sign = true;


void init(void)
{
    // select clearing color
    glClearColor(0.0, 0.0, 0.0, 0.0);

    // initialize viewing values
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, 30.0, 0.0, 30.0, -1.0, 1.0);

    glShadeModel(GL_SMOOTH);
}


void display(void)
{
    // change color intensity over time
    if (sign) {
        col += d_col;

        if (col >= 1.0) {
            col = 1.0;
            sign = false;
        }
    } else {
        col -= d_col;

        if (col <= 0.0) {
            col = 0.0;
            sign = true;
        }
    }


    // clear all pixels
    glClear(GL_COLOR_BUFFER_BIT);

    glBegin(GL_TRIANGLES);
    glColor3f(col, 0.0, 0.0);
    glVertex2f(5.0, 5.0);
    glColor3f(0.0, col, 0.0);
    glVertex2f(25.0, 5.0);
    glColor3f(0.0, 0.0, col);
    glVertex2f(5.0, 25.0);
    glEnd();

    // start processing buffered OpenGL routines
    glFlush();
}


void reshape(int w, int h)
{
    glViewport(0, 0, (GLsizei) w, (GLsizei) h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    if (w <= h) {
        gluOrtho2D(0.0, 30.0, 0.0, 30.0 * (GLfloat) h / (GLfloat) w);
    } else {
        gluOrtho2D(0.0, 30.0 * (GLfloat) w / (GLfloat) h, 0.0, 30.0);
    }

    glMatrixMode(GL_MODELVIEW);
}


void keyboard(unsigned char key, int x, int y)
{
    switch (key) {
        case 27:
            exit(0);
            break;
    }
}


int main(int argc, char ** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA);
    glutInitWindowSize(512, 512);
    glutInitWindowPosition(704, 250);
    glutCreateWindow("Learning OpenGL");

    init();

    glutDisplayFunc(display);
    glutIdleFunc(display);
//  glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutMainLoop();
    return 0;
}
