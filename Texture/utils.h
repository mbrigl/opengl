#ifndef LOADSHADER_H
#define LOADSHADER_H

#define GLEW_STATIC
#include "GL/glew.h"
#include <GLFW/glfw3.h>


GLFWwindow * initOpenGL(GLuint w, GLuint h, const char * name);

void key_callback(GLFWwindow * window, int key,
                  int scancode, int action, int mode);

GLuint loadShader(const char * vertex_path, const char * fragment_path);

GLuint loadTexture(const char * filepath);


GLfloat getMixValue();


#endif // LOADSHADER_H
