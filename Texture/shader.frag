#version 330 core
in vec3 ourColor;
in vec2 TexCoord;

out vec4 color;

// Texture samplers
uniform sampler2D texture1;
uniform sampler2D texture2;

uniform float weight;

void main()
{
    //  Mix the resulting texture color with the vertex colors
    //color = texture(texture1, TexCoord) * vec4(ourColor, 1.0f);

    // Linearly interpolate between both textures (weight refers to second parameter)
    color = mix(texture(texture1, TexCoord), texture(texture2, TexCoord), weight);

    // Exercise: flip face look direction:
    //color = mix(texture(texture1, TexCoord), texture(texture2, vec2(1.0 - TexCoord.x, TexCoord.y)), 0.2);
}
