#include <iostream>
#include <cmath>
#include "utils.h"

#define WIDTH 800
#define HEIGHT 600


// The MAIN function, from here we start the application and run the game loop
int main()
{
    GLFWwindow * window = initOpenGL(WIDTH, HEIGHT, "Textures");


    // Build and compile our shader program
    GLuint shader = loadShader("/mnt/sda3/GIT/OpenGL/Texture/shader.vert",
                               "/mnt/sda3/GIT/OpenGL/Texture/shader.frag");

    // Load and create textures
    GLuint texture1 = loadTexture("/mnt/sda3/GIT/OpenGL/Texture/container.jpg");
    GLuint texture2 = loadTexture("/mnt/sda3/GIT/OpenGL/Texture/awesomeface.png");


    // Set up vertex data (and buffer(s)) and attribute pointers
    GLfloat vertices[] = {
        // Positions          // Colors           // Texture Coords
        +0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f, // Top Right
        +0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, // Bottom Right
        -0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f, // Bottom Left
        -0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f  // Top Left
    };
    GLuint indices[] = {
        0, 1, 3, // First Triangle
        1, 2, 3  // Second Triangle
    };

    GLuint VBO, VAO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
                          (GLvoid *)0);
    glEnableVertexAttribArray(0);
    // Color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
                          (GLvoid *)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);
    // TexCoord attribute
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(GLfloat),
                          (GLvoid *)(6 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);

    glBindVertexArray(0); // Unbind VAO


    while (!glfwWindowShouldClose(window)) {
        // Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
        glfwPollEvents();

        // Render
        // Clear the colorbuffer
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // Activate shader
        glUseProgram(shader);

        // Update the uniform color
        GLint weight = glGetUniformLocation(shader, "weight");
        glUniform1f(weight, getMixValue());


        // Bind Textures using texture units (at least a minimum of 16 texture units)
        glActiveTexture(GL_TEXTURE0);  // Activate the texture unit before binding tx
        glBindTexture(GL_TEXTURE_2D, texture1);  // bind tx to the active texture unit
        // bind uniform "texture1" to texture unit 0
        glUniform1i(glGetUniformLocation(shader, "texture1"), 0);
        glActiveTexture(GL_TEXTURE1);  // Activate the texture unit before binding tx
        glBindTexture(GL_TEXTURE_2D, texture2);  // bind tx to the active texture unit
        // bind uniform "texture2" to texture unit 1
        glUniform1i(glGetUniformLocation(shader, "texture2"), 1);

        // Draw container
        glBindVertexArray(VAO);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);

        // Swap the screen buffers
        glfwSwapBuffers(window);
    }

    // Properly de-allocate all resources once they've outlived their purpose
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);

    // Terminate GLFW, clearing any resources allocated by GLFW.
    glfwTerminate();
    return 0;
}
