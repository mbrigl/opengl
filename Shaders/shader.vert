#version 330 core

// The position variable has attribute position 0
layout (location = 0) in vec3 position;
// The color variable has attribute position 1
layout (location = 1) in vec3 color;

out vec3 vertexColor; // Output a color to the fragment shader

void main()
{
    gl_Position = vec4(position, 1.0);

    // Set the output variable to the input color we got from the vertex data
    vertexColor = color;
}
