TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lglfw -lGL -lGLEW

SOURCES += \
    main.cpp \
    loadshader.cpp

HEADERS += \
    loadshader.h

OTHER_FILES += \
    shader.vert \
    shader.frag
