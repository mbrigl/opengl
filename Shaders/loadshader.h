#ifndef LOADSHADER_H
#define LOADSHADER_H

#include "GL/glew.h"

GLuint loadShader(const char * vertex_path, const char * fragment_path);

#endif // LOADSHADER_H
