#version 330 core

// Uniforms are global and can be accessed from any
// shader at any stage in the shader program
// uniform vec4 globalColor; // set in the OpenGL code.

in vec3 vertexColor;
out vec4 color;

void main()
{
    color = vec4(vertexColor, 1.0f);
} 
